CLI Merge
=========

Your own app can extend `acct`'s command line interface to use the `--acct-file` and `--acct-key` options.

my_project_root/my_project/conf.py

.. code-block:: python

    CLI_CONFIG = {
        "acct_file": {"source": "acct", "os": "ACCT_FILE"},
        "acct_key": {"source": "acct", "os": "ACCT_KEY"},
        "crypto_plugin": {"source": "acct"},
    }


Now in your app, load "acct" and ``hub.OPT.acct`` will be populated with the merged cli options from acct.

.. code-block:: python

    def cli(hub):
        hub.pop.config.load(["acct", "my_project"], cli="my_project")
        print(hub.OPT.acct.acct_FILE)
        print(hub.OPT.acct.acct_key)
        print(hub.OPT.acct.crypto_plugin)
