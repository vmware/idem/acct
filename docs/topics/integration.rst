Integration
===========

Add `acct` startup code to your project's initializer:

my_project/my_project/init.py

.. code-block:: python

    def __init__(hub):
        # Horizontally merge the acct dynamic namespace into your project
        hub.pop.sub.add(dyne_name="acct")


    def cli(hub):
        # Load the config from evbus onto hub.OPT
        hub.pop.config.load(["my_project", "acct"], cli="my_project")

        # decrypt the encrypted file using the given key and populate hub.acct.PROFILES with the decrypted structures
        hub.acct.init.unlock(hub.OPT.acct.acct_file, hub.OPT.acct.acct_key)

        # Process profiles from subs in `hub.acct.my_project` and put them into `hub.acct.SUB_PROFILES`
        # return the explicitly named profile
        profile = hub.acct.init.gather(
            subs=["my_project"], profile=hub.OPT.acct.acct_profile
        )

Alternatively, your app can collect profiles explicitly without storing them on the hub:

my_project/my_project/init.py

.. code-block:: python

    def __init__(hub):
        # Horizontally merge the acct dynamic namespace into your project
        hub.pop.sub.add(dyne_name="acct")


    async def cli(hub):
        # Load the config from my_project onto hub.OPT
        hub.pop.config.load(["my_project", "acct"], cli="my_project")

        # Collect profiles without storing them on the hub
        profiles = hub.acct.init.profiles(hub.OPT.acct.acct_file, hub.OPT.acct.acct_key)

        # Collect subs without storing them on the hub
        sub_profiles = await hub.acct.init.process(["my_project"], profiles)

        # Retrieve a specifically named profile
        profile = await hub.acct.init.single(
            profile_name=hub.OPT.acct.acct_profile,
            subs=["my_project"],
            sub_profiles=sub_profiles,
            profiles=profiles,
        )
