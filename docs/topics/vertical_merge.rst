Vertical App Merge
==================

In your own project, you can vertically merge `acct` and extend it with your own plugins:

my_project_root/my_project/conf.py

.. code-block:: python

    DYNE = {
        "acct": ["acct"],
    }

every file and directory added under ``my_project_root/my_project/acct/my_project`` will be available
in the respective path on the hub under ``hub.acct.my_project``
