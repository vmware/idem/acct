import pytest


def test_tpath(hub):
    """
    Verify that the custom plugin made it onto the hub
    """
    assert "provider" in hub.acct.metadata
    assert hub.acct.metadata.provider.gather


@pytest.mark.asyncio
async def test_gather(hub):
    """
    Test the the golden path where everything works properly
    """
    acct_metadata = await hub.acct.metadata.init.gather(
        providers=["provider"], profile={"foo": "bar"}
    )
    assert acct_metadata["account_id"]


@pytest.mark.asyncio
async def test_gather_no_provider(hub):
    """
    Test what happens when no provider is specified
    """
    acct_metadata = await hub.acct.metadata.init.gather(
        providers=[], profile={"foo": "bar"}
    )
    assert not acct_metadata


@pytest.mark.asyncio
async def test_gather_bad_provider(hub):
    """
    Test what happens when a nonexistant provider is specified
    """
    acct_metadata = await hub.acct.metadata.init.gather(
        providers=["foo"], profile={"foo": "bar"}
    )
    assert not acct_metadata


@pytest.mark.asyncio
async def test_gather_error_provider(hub):
    """
    Test what happens when metadata collection throws an error
    """
    acct_metadata = await hub.acct.metadata.init.gather(
        providers=["bad"], profile={"foo": "bar"}
    )
    assert not acct_metadata
