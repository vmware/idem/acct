import json
import subprocess
import sys


def test_extras(runpy):
    """
    Test that no value passed to extras becomes a dictionary
    """
    proc = subprocess.Popen(
        [
            sys.executable,
            runpy,
            "decrypt",
            "file",
            "--output=json",
            "--config-template",
        ],
        env={},
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )

    assert proc.wait() == 0, proc.stderr.read().decode()

    stdout = proc.stdout.read().decode()
    assert json.loads(stdout)["acct"]["extras"] == {}


def test_os_vars(runpy):
    """
    Test that os vars are read properly
    """
    proc = subprocess.Popen(
        [
            sys.executable,
            runpy,
            "decrypt",
            "file",
            "--output=json",
            "--config-template",
        ],
        env={"ACCT_FILE": "acct-file", "ACCT_KEY": "acct-key"},
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )

    assert proc.wait() == 0, proc.stderr.read().decode()

    stdout = proc.stdout.read().decode()
    assert json.loads(stdout)["acct"]["acct_file"] == "acct-file"
    assert json.loads(stdout)["acct"]["acct_key"] == "acct-key"
