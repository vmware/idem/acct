async def gather(hub, profiles):
    ret = {"default": {}}

    for profile, ctx in profiles.get("provider.test", {}).items():
        ctx["processed"] = True
        ret[profile] = ctx

    return ret
